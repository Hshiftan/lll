﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FinalBatchPlanning
{
    public partial class LocationInfo : UserControl
    {
        public LocationInfo()
        {
            InitializeComponent();
        }

        public void SetLocationText(string Txt)
        {
            Lbl_Loc.Text = Txt;
        }

        public void SetCatalogText(string Txt)
        {
            Lbl_Catalog.Text = Txt;
        }

        public void SetWeightText(string Txt)
        {
            Lbl_Weight.Text = Txt;
        }

        public void SetSurfaceText(string Txt)
        {
            Lbl_SurfaceNo.Text = Txt;
        }

        public void SetExpDateText(string Txt)
        {
            Lbl_ExpDate.Text = Txt;
        }

        public void SetLabelColor(Color C)
        {
            Lbl_Loc.BackColor = C;
            Lbl_Catalog.BackColor = C;
            Lbl_Weight.BackColor = C;
            Lbl_SurfaceNo.BackColor = C;
            Lbl_ExpDate.BackColor = C;
        }
    }        
}
