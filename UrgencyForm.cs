﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace FinalBatchPlanning
{
    public delegate void urgencyEvent(object source, urgentEventArgs args);//הכרזה על פונקציה 

    public partial class UrgencyForm : Form
    {
        private  int num;
        private string catlogform1;
        public event urgencyEvent UR;//הגדרת פונקציה urgency event



        public UrgencyForm(string catalog,int quantity)
        {
            InitializeComponent();
            if (cbox.SelectedIndex>-1)
                button1.Enabled = true;

            for (int i = 0; i < quantity; i++)
            {
                cbox.Items.Add(i+1);
            }
            label2.Text = catalog;
            catlogform1 = catalog;

        }

        
        private void UrgencyForm_Load(object sender, EventArgs e)
        {

        }

        private void gbox_Enter(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            num=int.Parse((cbox.SelectedItem.ToString()));
            urgentEventArgs uea = new urgentEventArgs { numberUrgency = num, catalog = catlogform1 };
            if (UR != null)
                UR(this, uea);//מפעיל את הפונקציה בפורם 1 שולח את כל האובייקט ואת uea
            this.Close();//לסגור פורם
            
          


        }
    }
}
