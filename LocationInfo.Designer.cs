﻿namespace FinalBatchPlanning
{
    partial class LocationInfo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Lbl_Loc = new System.Windows.Forms.Label();
            this.Lbl_Catalog = new System.Windows.Forms.Label();
            this.Lbl_Weight = new System.Windows.Forms.Label();
            this.Lbl_SurfaceNo = new System.Windows.Forms.Label();
            this.Lbl_ExpDate = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Lbl_Loc
            // 
            this.Lbl_Loc.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_Loc.Location = new System.Drawing.Point(0, 0);
            this.Lbl_Loc.Name = "Lbl_Loc";
            this.Lbl_Loc.Size = new System.Drawing.Size(160, 23);
            this.Lbl_Loc.TabIndex = 0;
            this.Lbl_Loc.Text = "Location";
            this.Lbl_Loc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Lbl_Catalog
            // 
            this.Lbl_Catalog.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_Catalog.Location = new System.Drawing.Point(0, 21);
            this.Lbl_Catalog.Name = "Lbl_Catalog";
            this.Lbl_Catalog.Size = new System.Drawing.Size(160, 20);
            this.Lbl_Catalog.TabIndex = 1;
            this.Lbl_Catalog.Text = "Catalog";
            this.Lbl_Catalog.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Lbl_Weight
            // 
            this.Lbl_Weight.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_Weight.Location = new System.Drawing.Point(0, 40);
            this.Lbl_Weight.Name = "Lbl_Weight";
            this.Lbl_Weight.Size = new System.Drawing.Size(160, 20);
            this.Lbl_Weight.TabIndex = 2;
            this.Lbl_Weight.Text = "Weight";
            this.Lbl_Weight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Lbl_SurfaceNo
            // 
            this.Lbl_SurfaceNo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_SurfaceNo.Location = new System.Drawing.Point(0, 58);
            this.Lbl_SurfaceNo.Name = "Lbl_SurfaceNo";
            this.Lbl_SurfaceNo.Size = new System.Drawing.Size(160, 20);
            this.Lbl_SurfaceNo.TabIndex = 3;
            this.Lbl_SurfaceNo.Text = "Surface No";
            this.Lbl_SurfaceNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Lbl_ExpDate
            // 
            this.Lbl_ExpDate.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_ExpDate.Location = new System.Drawing.Point(0, 75);
            this.Lbl_ExpDate.Name = "Lbl_ExpDate";
            this.Lbl_ExpDate.Size = new System.Drawing.Size(160, 20);
            this.Lbl_ExpDate.TabIndex = 4;
            this.Lbl_ExpDate.Text = "Exp Date";
            this.Lbl_ExpDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LocationInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.Lbl_ExpDate);
            this.Controls.Add(this.Lbl_SurfaceNo);
            this.Controls.Add(this.Lbl_Weight);
            this.Controls.Add(this.Lbl_Catalog);
            this.Controls.Add(this.Lbl_Loc);
            this.Name = "LocationInfo";
            this.Size = new System.Drawing.Size(160, 99);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label Lbl_Loc;
        private System.Windows.Forms.Label Lbl_Catalog;
        private System.Windows.Forms.Label Lbl_Weight;
        private System.Windows.Forms.Label Lbl_SurfaceNo;
        private System.Windows.Forms.Label Lbl_ExpDate;
    }
}
